# ps3-cell-sdks

## Cell Broadband Engine Software Developer Kits (Archive)

This repo contains every release of IBM's Software Development Kit (SDK) for the Cell Broadband Engine (that I am aware of). I use the most recent (3.1.0.0.0) on my jailbroke Playstation 3 in Fedora 12 (ppc64) Linux. It installs/works flawlessly - no headaches.

## PS3 Linux & Cell/B.E. on the Web

- Visit my PS3 Linux & Cell/B.E. website at [ps3linux.net](http://ps3linux.net) - hosted on my own jailbroke PS3 Linux web server (no joke).
- My PS3 Linux YouTube channel: [www.youtube.com/channel/UCt1mQjAb5WNd9fGsDX-rc9A](https://www.youtube.com/channel/UCt1mQjAb5WNd9fGsDX-rc9A)

### The Model Citizen
